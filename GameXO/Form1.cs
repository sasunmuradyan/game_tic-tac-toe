﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace GameXO
{
    public partial class Form1 : Form
    {
        static byte n = 20;
        byte[,] x0 = new byte[n, n];
        byte turn = 1;
        int winCountX = 0;
        int winCount0 = 0;

        Size boxSize = new Size(15, 15);
        Size SizeX0 = new Size(14, 14);
        Bitmap[,] gameState = new Bitmap[n, n];
        Bitmap imgNull;
        Bitmap imgX;
        Bitmap img0;
        Point mouseClickPosition = new Point(-1, -1);
        Bitmap gameMap;
        bool mouseDown = false;
        public Form1()
        {
            InitializeComponent();
            imgNull = new Bitmap(new FileStream("null.png", FileMode.Open));
            imgX = new Bitmap(new FileStream("x.png", FileMode.Open));
            img0 = new Bitmap(new FileStream("o.png", FileMode.Open));
            startNewGame();
        }
        void startNewGame()
        {
            n = Convert.ToByte(textBox1.Text);
            x0 = new byte[n, n];
            gameState = new Bitmap[n, n];
            pictureBox1.Enabled = true;
            turn = 1;
            labelTurn.Text = "X-ի հերթն է";
            labelTurn.ForeColor = Color.Red;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    x0[i, j] = 8;
                }
            }
            pictureBox1.Width = n * 15 + 2;
            pictureBox1.Height = n * 15 + 2;
            if (n >= 30)
            {
                this.Size = new Size(pictureBox1.Width + 45, pictureBox1.Height + 140);

                labelTurn.Location = new Point(15,labelTurn.Location.Y);
                groupBox1.Location = new Point(this.Size.Width / 2 - 30, groupBox1.Location.Y);
                groupBox2.Location = new Point(this.Size.Width - 133, groupBox2.Location.Y);

                button1.Location = new Point(this.Size.Width / 2 - 155, this.Size.Height - 80);
                button2.Location = new Point(this.Size.Width / 2 , this.Size.Height - 80);
            }
            else
            {
                this.Size = new Size(497, pictureBox1.Height + 140);

                labelTurn.Location = new Point(15, labelTurn.Location.Y);
                groupBox1.Location = new Point(218, groupBox1.Location.Y);
                groupBox2.Location = new Point(364, groupBox2.Location.Y);

                button1.Location = new Point(93, this.Size.Height - 80);
                button2.Location = new Point(248, this.Size.Height - 80);
            }
            
             
            for (int y = 0; y < gameState.GetLength(1); y++)
            {
                for (int x = 0; x < gameState.GetLength(0); x++)
                    gameState[x, y] = imgNull;
            }

            pictureBox1.Image = CreateImage();
            pictureBox1.MouseDown += new MouseEventHandler(pictureBox1_MouseDown);
            pictureBox1.MouseUp += new MouseEventHandler(pictureBox1_MouseUp);
        }
        void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                int X = e.X * n / pictureBox1.Width;
                int Y = e.Y * n / pictureBox1.Height;
                if (gameState[X, Y] == imgNull)
                {

                    labelTurn.Text = (turn == 0) ? "X-ի հերթն է" : "0-ի հերթն է";
                    labelTurn.ForeColor = (turn == 0) ? Color.Red : Color.Blue;

                    x0[Y, X] = turn;
                    checkWinDiagonal(Y, X, turn);
                    checkWinDiagonal2(Y, X, turn);
                    checkWinHorizontal(Y, X, turn);
                    checkWinVertical(Y, X, turn);
                    if (turn == 1)
                    {
                        gameState[X, Y] = imgX;
                    }
                    else
                    {
                        gameState[X, Y] = img0;
                    }
                    gameMap = AddImage(gameMap, new Bitmap(gameState[X, Y], SizeX0), new Point(pictureBox1.Size.Width / n * X + 1, pictureBox1.Size.Height / n * Y + 1));
                    pictureBox1.Image = gameMap;
                    turn = (byte)Math.Abs(1 - turn);
                }
                mouseDown = false;
            }
        }
        void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseClickPosition = e.Location;
        }
        public Bitmap CreateImage()
        {
            Bitmap map = new Bitmap(gameState.GetLength(0) * boxSize.Width, gameState.GetLength(1) * boxSize.Height);
            map = AddSetka(map, boxSize, Color.Black);
            //map = AddImages(gameState, map, boxSize);
            gameMap = map;
            return map;
        }
        public Bitmap AddSetka(Bitmap map, Size echeyka, Color colorLine)
        {
            Bitmap newMap = (Bitmap)map.Clone();
            for (int y = 0; y < map.Height; y++)
            {
                for (int x = 0; x < map.Width; x++)
                {
                    if (x % echeyka.Width == 0 || y % echeyka.Height == 0)
                    {
                        newMap.SetPixel(x, y, colorLine);
                    }
                }
            }
            return newMap;
        }
        //public Bitmap AddImages(Bitmap[,] game, Bitmap map, Size echeyka)
        //{
        //    Bitmap newMap = (Bitmap)map.Clone();
        //    for (int y = 0; y < map.Height; y++)
        //    {
        //        for (int x = 0; x < map.Width; x++)
        //        {
        //            if (x % echeyka.Width == 0 & y % echeyka.Height == 0)
        //            {
        //                int X = x / echeyka.Width;
        //                int Y = y / echeyka.Height;
        //                newMap = AddImage(newMap, new Bitmap(gameState[X, Y], new Size(echeyka.Width - 1, echeyka.Height - 1)), new Point(x + 1, y + 1));
        //            }
        //        }
        //    }
        //    return newMap;
        //}

        public Bitmap AddImage(Bitmap map, Bitmap image, Point point)
        {
            Bitmap newMap = (Bitmap)map.Clone();
            for (int y = point.Y; y < image.Height + point.Y & y < map.Height; y++)
            {
                for (int x = point.X; x < image.Width + point.X & x < map.Width; x++)
                {
                    
                    newMap.SetPixel(x, y, image.GetPixel(x - point.X, y - point.Y));
                }
            }
            return newMap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            startNewGame();
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //startNewGame();
        }

        void checkWinHorizontal(int i, int j, byte symbol)
        {
            byte startPoint;
            byte endPoint;
            if (i - 4 >= 0)
            {
                startPoint = (byte)(i - 4);
            }
            else
            {
                startPoint = 0;
            }
            if (i + 4 <= n - 1)
            {
                endPoint = (byte)(i + 4);
            }
            else
            {
                endPoint = (byte)(n - 1);
            }
            int count = 0;
            for (int k = startPoint; k <= endPoint; ++k)
            {
                if (x0[k, j] == symbol)
                {
                    count += 1;
                    if (count == 5)
                    {
                        if (turn == 1)
                        {
                            labelTurn.Text = "X-ը հաղթեց";
                            labelTurn.ForeColor = Color.Red;
                            winCountX++;
                            labelWinX.Text = Convert.ToString(winCountX);
                        }
                        else
                        {
                            labelTurn.Text = "0-ն հաղթեց";
                            labelTurn.ForeColor = Color.Blue;
                            winCount0++;
                            labelWin0.Text = Convert.ToString(winCount0);
                        }
                        pictureBox1.Enabled = false;
                    }
                }
                else
                {
                    count = 0;
                }
            }
        }
        void checkWinVertical(int i, int j, byte symbol)
        {
            byte startPoint;
            byte endPoint;
            if (j - 4 >= 0)
            {
                startPoint = (byte)(j - 4);
            }
            else
            {
                startPoint = 0;
            }
            if (j + 4 <= n - 1)
            {
                endPoint = (byte)(j + 4);
            }
            else
            {
                endPoint = (byte)(n - 1);
            }
            int count = 0;
            for (int k = startPoint; k <= endPoint; ++k)
            {
                if (x0[i, k] == symbol)
                {
                    count += 1;
                    if (count == 5)
                    {
                        if (turn == 1)
                        {
                            labelTurn.Text = "X-ը հաղթեց";
                            labelTurn.ForeColor = Color.Red;
                            winCountX++;
                            labelWinX.Text = Convert.ToString(winCountX);
                        }
                        else
                        {
                            labelTurn.Text = "0-ն հաղթեց";
                            labelTurn.ForeColor = Color.Blue;
                            winCount0++;
                            labelWin0.Text = Convert.ToString(winCount0);
                        }
                        pictureBox1.Enabled = false;
                    }
                }
                else
                {
                    count = 0;
                }
            }
        }

        void checkWinDiagonal(int i, int j, byte symbol)
        {
            byte startPointI;
            byte endPointI;
            byte startPointJ;
            byte endPointJ;
            if (i - 4 >= 0)
            {
                if (j - 4 >= 0)
                {
                    startPointI = (byte)(i - 4);
                    startPointJ = (byte)(j - 4);
                }
                else
                {
                    startPointI = (byte)(i - j);
                    startPointJ = 0;
                }

            }
            else
            {
                if (j - 4 >= 0)
                {
                    startPointI = 0;
                    startPointJ = (byte)(j - i);
                }
                else if (i > j)
                {
                    startPointI = (byte)(i - j);
                    startPointJ = 0;
                }
                else if (i < j)
                {
                    startPointI = 0;
                    startPointJ = (byte)(j - i);
                }
                else
                {
                    startPointI = 0;
                    startPointJ = 0;
                }
            }
            //end end end
            if (i + 4 <= n - 1)
            {
                if (j + 4 <= n - 1)
                {
                    endPointI = (byte)(i + 4);
                    endPointJ = (byte)(j + 4);
                }
                else
                {
                    endPointI = (byte)(i + ((n - 1) - j));
                    endPointJ = (byte)(n - 1);
                }
            }
            else
            {
                if (j + 4 <= n - 1)
                {
                    endPointI = (byte)(n - 1);
                    endPointJ = (byte)(j + ((n - 1) - i));
                }
                else if (i > j)
                {
                    endPointI = (byte)(n - 1);
                    endPointJ = (byte)(j + ((n - 1) - i));
                }
                else if (i < j)
                {
                    endPointI = (byte)(i + ((n - 1) - j));
                    endPointJ = (byte)(n - 1);
                }
                else
                {
                    endPointI = (byte)(n - 1);
                    endPointJ = (byte)(n - 1);
                }
            }
            //MessageBox.Show(String.Format("I={0} , {1}  J= {2} , {3}", startPointI, endPointI, startPointJ, endPointJ));
            int count = 0;
            for (int I = startPointI, J = startPointJ; I <= endPointI; ++I, ++J)
            {
                if (x0[I, J] == symbol)
                {
                    count += 1;
                    if (count == 5)
                    {
                        if (turn == 1)
                        {
                            labelTurn.Text = "X-ը հաղթեց";
                            labelTurn.ForeColor = Color.Red;
                            winCountX++;
                            labelWinX.Text = Convert.ToString(winCountX);
                        }
                        else
                        {
                            labelTurn.Text = "0-ն հաղթեց";
                            labelTurn.ForeColor = Color.Blue;
                            winCount0++;
                            labelWin0.Text = Convert.ToString(winCount0);
                        }
                        pictureBox1.Enabled = false;
                    }
                }
                else
                {
                    count = 0;
                }
            }
        }
        void checkWinDiagonal2(int i, int j, byte symbol)
        {
            byte startPointI = 255;
            byte endPointI = 255;
            byte startPointJ = 255;
            byte endPointJ = 255;
            if ((i >= 4) && (i <= n - 5) && (j >= 4) && (j <= n - 5))
            {
                //MessageBox.Show("1");
                startPointI = (byte)(i - 4);
                startPointJ = (byte)(j - 4);
                endPointI = (byte)(i + 4);
                endPointJ = (byte)(j + 4);
            }
            else if ((i < 4) && (j >= 4) && (j <= n - 5))
            {
                //MessageBox.Show("2");
                startPointI = 0;
                startPointJ = (byte)(j - 4);
                endPointI = (byte)(i + 4);
                endPointJ = (byte)(j + i);
            }
            else if ((i >= 4) && (i <= n - 5) & (j > n - 5))
            {
                //MessageBox.Show("3");
                startPointI = (byte)(i - ((n - 1) - j));
                startPointJ = (byte)(j - 4);
                endPointI = (byte)(i + 4);
                endPointJ = (byte)(n - 1);

            }
            else if ((i > n - 5) && (j >= 4) && (j <= n - 5))
            {
                //MessageBox.Show("4");
                startPointI = (byte)(i - 4);
                startPointJ = (byte)(j - ((n - 1) - i));
                endPointI = (byte)(n - 1);
                endPointJ = (byte)(j + 4);
            }
            else if ((i >= 4) && (i <= n - 5) & (j < 4))
            {
                //MessageBox.Show("5");
                startPointI = (byte)(i - 4);
                startPointJ = 0;
                endPointI = (byte)(i + j);
                endPointJ = (byte)(j + 4);
            }
            else if ((i >= 0) && (i <= 3) && (j >= 0) && (j <= 3))
            {
                //MessageBox.Show("6");
                startPointI = 0;
                startPointJ = 0;
                endPointI = (byte)(i + j);
                endPointJ = (byte)(j + i);
            }
            else if ((i >= 0) && (i <= 3) && (j >= n - 4) && (j <= n - 1))
            {
                //MessageBox.Show("7");
                startPointI = (i + j) <= (n - 1) ? (byte)0 : (byte)((n - 1) + i - j);
                startPointJ = (byte)(j - 4);
                endPointI = (byte)(i + 4);
                endPointJ = (i + j) >= (n - 1) ? (byte)(n - 1) : (byte)(i + j);
            }
            else if ((i >= n - 4) && (i <= n - 1) && (j >= 0) && (j <= 3))
            {
                //MessageBox.Show("8");
                startPointI = (byte)(i - 4);
                startPointJ = (i + j) <= (n - 1) ? (byte)0 : (byte)((n - 1) - i + j);
                endPointI = (i + j) >= (n - 1) ? (byte)(n - 1) : (byte)(i + j);
                endPointJ = (byte)(j + 4);
            }
            else if ((i >= n - 4) && (i <= n - 1) && (j >= n - 4) && (j <= n - 1))
            {
                //MessageBox.Show("9");
                startPointI = (byte)(i - ((n - 1) - j));
                startPointJ = (byte)(j - ((n - 1) - i));
                endPointI = (byte)(n - 1);
                endPointJ = (byte)(n - 1);
            }
            //MessageBox.Show(String.Format("I={0} , {1}  J= {2} , {3}", startPointI, endPointI, startPointJ, endPointJ));
            int count = 0;
            for (int I = endPointI, J = startPointJ; J <= endPointJ; --I, ++J)
            {
                if (x0[I, J] == symbol)
                {
                    count += 1;
                    if (count == 5)
                    {
                        if (turn == 1)
                        {
                            labelTurn.Text = "X-ը հաղթեց";
                            labelTurn.ForeColor = Color.Red;
                            winCountX++;
                            labelWinX.Text = Convert.ToString(winCountX);
                        }
                        else
                        {
                            labelTurn.Text = "0-ն հաղթեց";
                            labelTurn.ForeColor = Color.Blue;
                            winCount0++;
                            labelWin0.Text = Convert.ToString(winCount0);
                        }
                        pictureBox1.Enabled = false;
                    }
                }
                else
                {
                    count = 0;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            winCount0 = 0;
            winCountX = 0;
            labelWinX.Text = Convert.ToString(winCountX);
            labelWin0.Text = Convert.ToString(winCount0);
        }
    }
}
